import './App.css'
import UserComment from './components/UserComment'
import Blog from './components/Blog'

function App() {
  const userComment = {
    username: 'amit',
    date: 'Dec 15, 2021',
    details:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
  }

  const comments = [
    {
      username: 'user1',
      date: 'Dec 10, 2021',
      details: 'This is user 1 comment on Dec 10, 2021',
    },
    {
      username: 'user2',
      date: 'Dec 11, 2021',
      details: 'This is user 2 comment on Dec 11, 2021',
    },
    {
      username: 'user3',
      date: 'Dec 12, 2021',
      details: 'This is user 3 comment on Dec 12, 2021',
    },
  ]

  const countries = ['India', 'USA', 'UK', 'Japan']

  const blogs = [
    {
      username: 'user1',
      date: 'Dec 11, 2021',
      title: 'test blog 1',
      contents: 'this is content for blog 1',
    },
    {
      username: 'user2',
      date: 'Dec 11, 2021',
      title: 'test blog 2',
      contents: 'this is content for blog 2',
    },
    {
      username: 'user3',
      date: 'Dec 11, 2021',
      title: 'test blog 3',
      contents: 'this is content for blog 3',
    },
  ]

  return (
    <div>
      {/*
        UserComment({comment: userComment}) {
          return  
            <div className="user-comment">
              <div className="user-details">
                {username} on {date}
              </div>
              <div className="comment">{details}</div>
            </div>
        }
      */}
      {/* <UserComment comment={userComment} /> */}

      {/* {countries.map((country) => {
        return <div>{country}</div>
      })} */}

      {/* <div>{countries[0]}</div>
      <div>{countries[1]}</div>
      <div>{countries[2]}</div>
      <div>{countries[3]}</div> */}

      {/* {comments.map((tmpComment) => {
        return <UserComment comment={tmpComment} />
      })} */}

      {/* {comments.map((comment) => {
        return <UserComment comment={comment} />
      })} */}

      {blogs.map((blog) => {
        return <Blog blog={blog} />
      })}
    </div>
  )
}

export default App
