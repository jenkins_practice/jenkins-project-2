import './Blog.css'

const Blog = (props) => {
  const { title, username, date, contents } = props.blog
  return (
    <div className="blog">
      <div className="blog-title">{title}</div>
      <div className="date">
        Created by {username} on {date}
      </div>
      <div className="blog-contents">{contents}</div>
    </div>
  )
}

export default Blog
