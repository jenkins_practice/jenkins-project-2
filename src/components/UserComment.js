// functional component
const UserComment = (props) => {
  // const username = props.comment.username
  // const date = props.comment.date
  // const details = props.comment.details
  const { username, date, details } = props.comment

  return (
    <div className="user-comment">
      <div className="user-details">
        {username} on {date}
      </div>
      <div className="comment">{details}</div>
    </div>
  )
}

export default UserComment
